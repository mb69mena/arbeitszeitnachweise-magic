# arbeitszeitnachweise-magic

- Do you run linux (or just not windows)?
- Are you too lazy to use libreoffice?
- Do you hate using archaic tools? Like pens?
- Do you work for the university Leipzig and have to fill in these unnecessary forms just to prove that you are indeed not earning enough?

**Meet** <span style="color:red">arbeitszeitnachweise-magic</span>!

## Dependencies
- Python 3.7
- reportlab
- pypdf2
- pyyaml

Installed using `pip install --user reportlab pypdf2 pyyaml` or via pipenv:
```
 > pipenv install
```

## Workflow
The program will query the user for contract information on first run or whenever `--forget`
deleted all information.

To lazily **create all pdfs for the current job** run:
```
 > pipenv run main create -da
```

To **add working time information** run:
```
 > pipenv run main add -s 09:00 -e 11:30 -j 'Did the work'
```

Or alternatively, let the program query for `--start` and `--end`:
```
 > pipenv run main add -ij 'Did the work'
```

Or if you forgot to add your working hours from last christmas run:
```
 > pipenv run main add -ij 'Having no fun' -d 24.12
```

Or was it last years christmas?
```
 > pipenv run main add -j 'Having no fun at all' -d 24.12 -y 2018
```

To **create the final pdf** for last months work run:
```
 > pipenv run main create
```

To **get help** run one of these:
```
 > pipenv run main --help
 > pipenv run main create --help
 > pipenv run main add --help
```

## Example

```
 > pipenv run main --forget create -db 19:23 -m 02.19
We cannot find your contract information, please answer a few questions.

Last name, First name: Mustermann, Max
Institute: Institute of the Universe
Superior: Prof Patternman
Contract ID: 123456
Hours per weekly: 05:00
Workdays (MO,DI,MI,DO,FR,SA,SO): MO,SA
Start MO (HH:MM): 09:00
Ende MO (HH:MM): 11:30
Start SA (HH:MM): 16:54
Ende SA (HH:MM): 19:34
Start of contract (DD.MM.YYYY): 02.01.2019
End of contract (DD.MM.YYYY): 19.03.2020
```

The above will result in a pdf with the following content:

![example](/example/example.jpg)

See pdf [here](/example/example.pdf).

## Bugs

Please report all bugs and errors [here](https://git.informatik.uni-leipzig.de/mb69mena/arbeitszeitnachweise-magic).

## Development

Clone this repo and cd into the project:
```
 > git clone git@git.informatik.uni-leipzig.de:mb69mena/arbeitszeitnachweise-magic.git
 > cd arbeitszeitnachweise-magic
```
And create the virtual environment and install dependencies using `pipenv`:
```
 > pipenv install
```
The project can then be run using:
```
 > pipenv run python3 [file]
```

## Documentation

The project is documented using [spinx](http://www.sphinx-doc.org/en/stable/). After installing everything (see [Development](##Development)), just run:
```
 > pipenv run make -C doc html
```
The documentation should then be available in `doc/build/html/`.

