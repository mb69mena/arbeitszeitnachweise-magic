Welcome to Nachweise's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Introduction
=====================================

Welcome to the documentation for the arbeitszeitnachweise-magic project.
The source code is available here (link to gitlab), if you have access to the University
of Leipzig Computer Science faculty GitLab. 
The projects aim is to simplify the process of filling out your proof of work certifificates,
which is mandatory if you're working at the University as a so called studentische Hilfskraft 
(SHK).

Save your work
=====================================

The programm offers two main way for providing information about the times you have worked.

1. Manually insert them from the command line.
2. Automatic insertion via predefined weekly working days.
   These are specified in a contract.yml file.

The interaction with this file happens through these two methods.

.. autofunction:: src.io_util.check_contract
   
.. autofunction:: src.io_util.read_contract

For manually inserting your work into the working_times.yml file run the program with the
add parameter, this invokes the exec_add method.

.. autofunction:: src.main.exec_add

There are various flags that can be set.

+---------------------------+----------------------------------------------------------+
| Flag                      |                                                          |
+===========================+==========================================================+
| ``-i``                    | Interactive mode that asks you for necessary information |
+---------------------------+----------------------------------------------------------+
| ``-d --date``             | On wich day have you worked                              |
+---------------------------+----------------------------------------------------------+
| ``-s --start``            | When did you start working                               |
+---------------------------+----------------------------------------------------------+
| ``-e --end``              | When did you stop working                                |
+---------------------------+----------------------------------------------------------+
| ``--start_break``         | When did you start after your break                      |
+---------------------------+----------------------------------------------------------+
| ``--end_break``           | When did you stop after your break                       |
+---------------------------+----------------------------------------------------------+
| ``-j --justification``    | Why did you work                                         |
+---------------------------+----------------------------------------------------------+

Create output
===================================================================================

After all work entries have been added, the next step is to run the program with the create
parameter, this invokes the exec_create method.

.. autofunction:: src.main.exec_create

There are again multiple flags to choose from

+------------------------+----------------------------------------------------------------------+
| Flag                   | Meaning                                                              |
+========================+======================================================================+
| ``-t --timefile``      | Specifiy the location of your time file                              |
+------------------------+----------------------------------------------------------------------+
| ``-c --contractfile``  | Specify the location of your contract file                           |
+------------------------+----------------------------------------------------------------------+
| ``--forget``           | Delete your data, combine with other flags                           |
+------------------------+----------------------------------------------------------------------+
| ``-y --year``          | Specify the year in which you have worked                            |
+------------------------+----------------------------------------------------------------------+
| ``--from-month``       | The first month of the period you want to specify                    |
+------------------------+----------------------------------------------------------------------+
| ``--to-month``         | The last month of the perios you want to specify                     |
+------------------------+----------------------------------------------------------------------+
| ``-m --month``         | Specify a month, only works if --from-month and --to-month are empty |
+------------------------+----------------------------------------------------------------------+
| ``-d --defaults``      | Suppose you worked on all days specified in your contract            |
+------------------------+----------------------------------------------------------------------+
| ``-b --start-balance`` | Specify the balance from your last month                             |
+------------------------+----------------------------------------------------------------------+
| ``-f --font``          | Which font would you like to use                                     |
+------------------------+----------------------------------------------------------------------+
| ``--fontsize``         | Which fontsize would you like to use                                 |
+------------------------+----------------------------------------------------------------------+
| ``--fontsize``         | Which fontsize would you like to use                                 |
+------------------------+----------------------------------------------------------------------+
| ``o --output``         | Where would you like to store the resulting pdf                      |
+------------------------+----------------------------------------------------------------------+

Writing on the pdf
=================================================================================================
The University of Leipzig provides empty pdf, for you to fill in your working hours after printing
them. The raw pdfs for each year are stored in the ./data/ directory. We use the two python modules
`PyPDF2 <https://pypi.org/project/PyPDF2/>`_ and `reportlab <https://pypi.org/project/reportlab/>`_.
The idea is that you have the original pdf and an empty pdf linked to a canvas object, which you 
can draw and write on. After you are done filling in your work information on the empty canvas you 
merge the original pdf with the canvas. Methods for this are found in the pdf_util module.

.. automodule:: src.pdf_util 
   :members:
   :special-members:
   :private-members:

