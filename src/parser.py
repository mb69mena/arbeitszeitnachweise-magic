from reportlab.pdfgen import canvas
import io

import argparse
from datetime import date, timedelta


def init_parser() -> argparse.ArgumentParser:
    """Initialize argparse parser"""
    parser = argparse.ArgumentParser(
        description="""Automatically create working time evidence
            (Arbeitszeitnachweise) when working for 'Universität Leipzig'.""")
    parser.add_argument(
        "-t",
        "--timefile",
        type=str,
        help="where to store/read working times to/from",
        default="./working_times.yml")
    parser.add_argument(
        "-c",
        "--contractfile",
        type=str,
        help="file to store/read contract information to/from",
        default="./contract.yml")
    parser.add_argument(
        "-y",
        "--year",
        type=int,
        help="which year to assume",
        default=date.today().year)
    parser.add_argument(
        "--forget",
        help="FORGET ALL stored DATA. You may not want this",
        action="store_true")
    subs = parser.add_subparsers(required=True, dest="command")
    parser_create = subs.add_parser("create", help="create the final pdf")
    parser_create.add_argument(
        "--from-month",
        type=str,
        help=
        "(MM.YY) First month (inclusive) for which to create output. Use with --to-month"
    )
    parser_create.add_argument(
        "--to-month",
        type=str,
        help=
        "(MM.YY) Last month (inclusive) for which to create output. Use with --to-month"
    )
    parser_create.add_argument(
        "-m",
        "--month",
        type=str,
        help=
        "(MM.YY) Month for which to create output. If --from-month and --to-month are given, this will be ignored",
        default=__lastmonth__())
    parser_create.add_argument(
        "-d",
        "--defaults",
        help=
        "Automatically add preferred working times from contractfile to pdf",
        action="store_true")
    parser_create.add_argument(
        "-a",
        "--all",
        help="Create pdfs for all months specified in the contract",
        action="store_true")
    parser_create.add_argument(
        "-b",
        "--start-balance",
        type=str,
        help="(HH:MM) Set the starting balance for the first month",
        default="00:00")
    parser_create.add_argument(
        "--font",
        type=str,
        help="Specify a custom font to use in pdfs",
        default="Helvetica",
        choices=__get_available_fonts__())
    parser_create.add_argument(
        "--fontsize",
        type=int,
        help="Specify a custom font size to use in pdfs",
        default=7)
    parser_create.add_argument(
        "-o",
        "--output",
        type=str,
        help="where to write the finished pdf to",
        default="./Arbeitszeitnachweis.pdf")
    parser_add = subs.add_parser("add", help="add working time")
    parser_add.add_argument(
        "-i",
        "--interactive",
        help="Ask for missing data interactively",
        action="store_true")
    parser_add.add_argument("-d", "--date", help="(DD.MM) Day of work")
    parser_add.add_argument("-s", "--start", help="(HH:MM) Start of work")
    parser_add.add_argument(
        "-e", "--end", help="(HH:MM) End of work (before break)")
    parser_add.add_argument(
        "--start_break", help="(HH:MM) Start of work after break")
    parser_add.add_argument(
        "--end_break", help="(HH:MM) End of work (after break)")
    parser_add.add_argument(
        "-j",
        "--justification",
        type=str,
        help="Short justification",
        default="")
    parser_add.add_argument(
        "--mandatory",
        type=bool,
        help="Werer these working hours mandatory?",
        default=False
    )
    parser_test = subs.add_parser(
        "test", help="create a test pdf with fake data to verify quality")
    parser_test.add_argument(
        "--font",
        type=str,
        help="Specify a custom font to use in pdfs",
        default="Helvetica",
        choices=__get_available_fonts__())
    parser_test.add_argument(
        "--fontsize",
        type=int,
        help="Specify a custom font size to use in pdfs",
        default=7)
    parser_test.add_argument(
        "-o",
        "--output",
        type=str,
        help="where to write the finished pdf to",
        default="./Arbeitszeitnachweis.pdf")
    return parser


def __lastmonth__():
    today = date.today()
    today.replace(day=1)
    last_month = today - timedelta(3)
    return last_month.strftime("%m.%y")


def __get_available_fonts__():
    """ Return a list of available fonts """
    return canvas.Canvas(io.BytesIO).getAvailableFonts()
