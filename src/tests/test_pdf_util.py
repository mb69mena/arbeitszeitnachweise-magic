import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

import unittest
import pdf_util
from datetime import time
from reportlab.lib.units import inch


class PdfUtilTest(unittest.TestCase):
    """
        Test pdf_util.py functionality
    """

    def test__format_balance__(self):
        self.assertEqual("+00:00", pdf_util.__format_balance__(0))
        self.assertEqual("+01:00", pdf_util.__format_balance__(60))
        self.assertEqual("+99:59", pdf_util.__format_balance__(99 * 60 + 59))
        self.assertEqual("-00:01", pdf_util.__format_balance__(-1))
        self.assertEqual("-01:00", pdf_util.__format_balance__(-60))
        self.assertEqual("-99:59", pdf_util.__format_balance__(-99 * 60 - 59))

    def test__format_actual__(self):
        self.assertEqual("00:00", pdf_util.__format_actual__(0))
        self.assertEqual("01:00", pdf_util.__format_actual__(60))
        self.assertEqual("99:59", pdf_util.__format_actual__(99 * 60 + 59))

    def test__month_length__(self):
        months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        for i in range(12):
            self.assertEqual(months[i], pdf_util.__month_length__(i))

    def test__time_distance__(self):
        self.assertEqual(
            61, pdf_util.__time_distance__(time(11, 30), time(12, 31)))

    def test__flip__(self):
        A4_LONG_SIDE = 11.69 * inch
        self.assertEqual(A4_LONG_SIDE - 13.33, pdf_util.__flip__(13.33))


if __name__ == "__main__":
    unittest.main()
