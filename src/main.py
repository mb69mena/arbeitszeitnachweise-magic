#!/usr/bin/env python5

import argparse
import yaml
from datetime import datetime, date, timedelta, time
from os.path import isfile
from sys import exit, stderr
from io_util import *
from date_util import *
from parser import init_parser
from pdf_util import Merger
from creator import Creator

def ask_for_if_missing(args: argparse.Namespace, prop: str,
                       prompt: str) -> str:
    """
        queries the user for information that is mandatory and was not
        supplied in the command line or the config files
    """
    if vars(args)[prop]:
        return vars(args)[prop]
    elif args.interactive:
        return input(prompt + " ")
    else:
        print(
            "Missing information and --interactive was not specified!",
            file=stderr)
        exit(1)

def exec_add(args: argparse.Namespace):
    """ Add time object to timefile """
    with open(args.timefile) as timefile:
        time_objects = yaml.load(timefile) or []
        new_obj = {}
        date_to_use = date.today()
        if args.date:
            date_to_use = datetime.strptime(args.date, "%d.%m")
            date_to_use = date_to_use.replace(year=args.year)
        # Start of work
        start = ask_for_if_missing(args, "start",
                                   "When did you start (HH:MM)?")
        start = datetime.strptime(start, "%H:%M")
        new_obj["start"] = datetime.combine(date_to_use, start.time())
        # End of work
        end = ask_for_if_missing(args, "end", "When did you stop (HH:MM)?")
        end = datetime.strptime(end, "%H:%M")
        new_obj["end"] = datetime.combine(date_to_use, end.time())
        # Start of work after break
        if args.start_break:
            start_break = datetime.strptime(args.start_break, "%H:%M")
            new_obj["start_break"] = datetime.combine(date_to_use,
                                                      start_break.time())
        # End of work after break
        if args.end_break:
            end_break = datetime.strptime(args.end_break, "%H:%M")
            new_obj["end_break"] = datetime.combine(date_to_use,
                                                    end_break.time())
        # Justifications
        if args.justification:
            new_obj["justification"] = args.justification
        
        #Mandatory
        if args.mandatory:
            new_obj["mandatory"] = args.mandatory

        time_objects.append(new_obj)

    with open(args.timefile, 'w') as timefile:
        yaml.dump(time_objects, timefile, default_flow_style=False)

def exec_create(args: argparse.Namespace):
    """Create the final pdf using the given information from the working_times file"""
    creator = Creator(args)
    creator.create()
    
def to_weekday(day: int):
    if day == 0:
        return 'MO'
    if day == 1:
        return 'DI'
    if day == 2:
        return 'MI'
    if day == 3:
        return 'DO'
    if day == 4:
        return 'FR'
    if day == 5:
        return 'SA'
    if day == 6:
        return 'SO'

def get_month_year_list(start_year, start_month, end_year, end_month):
    res = []
    for y in range(start_year, end_year + 1):
        if y == end_year:
            upper = end_month
        else:
            upper = 12
        if y == start_year:
            lower = start_month
        else:
            lower = 1
        for m in range(lower, upper + 1):
            res.append((m, y))
    return res

def exec_test(args: argparse.Namespace):
    """Execute a simple test to verify the correct writing of the pdf"""
    balance = 0
    merger = Merger(args)
    for month in range(1, 13):
        mon_can = merger.begin_month(date(2019, month, 1), balance)
        mon_can.write_name("Max Mustermann")
        mon_can.write_contract_id("293843")
        mon_can.write_institute("Institute of great things")
        mon_can.write_weekly_hours("15:00")
        mon_can.write_superior("Master Mustermann")
        mon_can.write_working_days("MO, FR")
        mon_can.write_contract_start(date(1998, 2, 19))
        mon_can.write_contract_end(date(2023, 6, 11))
        month_date = date(args.year, month, 1)
        for day in range(1, days_of_month(month_date) + 1):
            mon_can.write_line(
                date(2019, month, day), time(9, 0), time(9, 1), time(15, 0),
                time(15, 1), "I hate my job")
        balance = mon_can.apply()
    merger.finalize()


if __name__ == "__main__":
    parser = init_parser()
    args = parser.parse_args()

    # Check sanity of contractfile
    check_contract(args)

    # Delete data if --forget was specified
    if args.forget:
        os.remove(args.timefile)
        os.remove(args.contractfile)

    # TODO: Heavily improve this garbage
    if not isfile(args.timefile):
        with open(args.timefile, 'w') as f:
            yaml.dump([], f)
    {
        "add": exec_add,
        "create": exec_create,
        "test": exec_test
    }[args.command](args)
