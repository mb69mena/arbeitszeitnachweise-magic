from reportlab.lib.units import inch
import argparse
from datetime import date, datetime
from reportlab.pdfgen import canvas
import yaml
import os.path

A4_LONG_SIDE = 11.69 * inch


def read_config(args: argparse.Namespace):
    """
        Reads the cofigfile (YAML), which stores information
        about the PDFs layout
    """
    configfile = "./data/" + str(args.year) + "/config.yml"
    with open(configfile) as f:
        config = yaml.load(f)
        return config


def read_contract(args: argparse.Namespace):
    """
        Reads the contract.yml file, which stores information
        about the users contract, working hours, contract no., etc.
    """
    contractfile = args.contractfile
    with open(contractfile) as f:
        contract = yaml.load(f)
        return contract


def check_contract(args: argparse.Namespace):
    """
        Checks whether a contractfile alredy exists and otherwise queries the
        user for the necessary information. Also warns if settings are outdated.
    """
    if not os.path.isfile(args.contractfile):
        # prompt user for contract information
        print(
            "We cannot find your contract information, please answer a few questions."
        )
        print()
        contract = {}
        contract['name'] = input('Last name, First name: ')
        contract['institute'] = input('Institute: ')
        contract['superior'] = input('Superior: ')
        contract['contract_id'] = input('Contract ID: ')
        contract['weekly_hours'] = input('Hours per weekly: ')
        days = input('Workdays (MO,DI,MI,DO,FR,SA,SO): ')
        contract['working_days'] = days.upper()
        days = days.split(',')
        for d in days:
            start = input(f'Start {d} (HH:MM): ')
            end = input(f'Ende {d} (HH:MM): ')
            start = datetime.strptime(start, "%H:%M")
            end = datetime.strptime(end, "%H:%M")
            contract[d] = {}
            contract[d]['start'] = start
            contract[d]['end'] = end
        contract_start = input('Start of contract (DD.MM.YYYY): ')
        contract_end = input('End of contract (DD.MM.YYYY): ')
        contract['contract_start'] = datetime.strptime(contract_start,
                                                       "%d.%m.%Y").date()
        contract['contract_end'] = datetime.strptime(contract_end,
                                                     "%d.%m.%Y").date()
        with open(args.contractfile, 'w') as f:
            yaml.dump(contract, f, default_flow_style=False)
    else:
        contract = read_contract(args)
        if contract["contract_end"] < date.today():
            choice = input("Contract information are outdated. Update? (y/N)")
            if choice == "y" or choice == "Y":
                os.remove(args.contractfile)
                check_contract(args)


def read_working_times(args: argparse.Namespace):
    with open(args.timefile) as f:
        return (yaml.load(f))
