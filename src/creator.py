from reportlab.pdfgen import canvas
from io_util import read_contract, check_contract, read_working_times
from pdf_util import Merger
from date_util import days_of_month
from datetime import datetime, date, time, timedelta
import argparse

class Creator(object):
    """A Creator handles all the logic necessary for a complete pdf"""
    def __init__(self, args: argparse.Namespace):
        super(Creator, self).__init__()
        self.args = args
        check_contract(args)
        self.contract = read_contract(self.args)
        self.contract_start = self.contract.get('contract_start', datetime.today())
        self.contract_end = self.contract.get('contract_end', datetime.today())
        self.last_balance = int(args.start_balance[:2] * 60)
        self.last_balance += int(args.start_balance[3:])
        self.working_times = read_working_times(args)
        self.working_times = sorted(self.working_times, key=lambda t: t['start'])
        self.merger = Merger(self.args)

    def create(self):
        # Get months to be convered in the pdf
        months_years = self.__get_month_year_list__(self.args)
        for (m, y) in months_years:
            try:
                month = self.merger.begin_month(date(y, m, 1), self.last_balance)
            except ValueError as e:
                print(e)
                continue
            self.__write_meta__(month)
            manuals = self.__get_manuals__(m, y, month)
            mandatories = self.__get_mandatories__(m, y, month)
            month_times = sorted(manuals + mandatories, key=lambda t: t['start'])
            self.__write__(month_times, month)
            month.apply()
            self.merger.finalize()

    def __get_mandatories__(self, month, year, writer):
        """
            Get all mandatory working times
        """
        if self.args.defaults:
            days = self.contract['working_days'].split(',')
        else:
            days = []
        month_date = date(year, month, 1)
        res = []
        for d in range(1, days_of_month(month_date) + 1):
            day = datetime.strptime(f'{d}.{month}.{year-2000}', '%d.%m.%y')
            day_string = self.__to_weekday__(day.weekday())
            if day_string in days and \
               day.date() >= self.contract_start and \
               day.date() <= self.contract_end:
                start = self.contract[day_string]['start'].replace(
                    year=year, month=month, day=day.day
                )
                end = self.contract[day_string]['end'].replace(
                    year=year, month=month, day=day.day        
                )
                res.append({'start': start, 'end': end, 'mandatory': True})
        return res
            
    def __get_manuals__(self, month, year, writer):
        """
            Get all manually inserted working times for ceratin month and year
        """
        month_times = [\
            t for t in self.working_times \
            if t['start'].month == month and t['start'].year == year \
        ]
        return month_times
        
    def __write__(self, times, writer):
        quota = timedelta(0)
        actual = timedelta(0)
        balance = timedelta(0)
        temps_wrote = False
        old_date = times[0]['start'].date().replace(day=1)
        for t in times:
            date = t['start'].date()
            for d in range(old_date.day+1, date.day):
                writer.write_line(
                    date.replace(day=d), time.min, time.min,
                    balance=self.__format_balance__(balance)
                )
            # Write temp row
            if date.day >= 16 and not temps_wrote:
                writer.write_temp_balance(self.__format_balance__(balance))
                writer.write_temp_actual(self.__format_actual__(actual))
                writer.write_temp_quota(self.__format_actual__(quota))
                temps_wrote = True
            start_time = t['start'].time()
            end_time = t['end'].time()
            start_break = t.get('start_break', None)
            end_break = t.get('end_break', None)
            justification = t.get('justification', '')
            mandatory = t.get('mandatory', False)
            if self.contract_start <= date and date <= self.contract_end:
                new_actual = \
                    (t['end'] - t['start']) + \
                    (t.get('end_break', timedelta(0)) - t.get('start_break', timedelta(0)))
                actual += new_actual
                if mandatory:
                    new_quota = new_actual
                    quota += new_quota
                    new_balance = timedelta(0)
                else:
                    new_quota = timedelta(0)
                    new_balance = new_actual
                    balance += new_actual
                writer.write_line(
                    date, start_time, end_time, start_break, end_break,
                    justification, mandatory, self.__format_actual__(new_actual),
                    self.__format_actual__(new_quota), self.__format_balance__(balance)
                )
            old_date = date
        writer.write_final_actual(self.__format_actual__(actual))
        writer.write_final_quota(self.__format_actual__(quota))
        writer.write_final_balance(self.__format_balance__(balance))

    def __format_balance__(self, times):
        hours = abs(times.days) * 24 + abs(times.seconds) // 3600
        minutes = (abs(times.seconds) // 60) % 60
        if times.seconds < 0:
            prefix = "-"
        else:
            prefix = "+"
        return "{}{:0>2}:{:0>2}".format(prefix, hours, minutes)

    def __format_actual__(self, times):
        hours = times.days * 24 + times.seconds // 3600
        minutes = (times.seconds // 60) % 60
        return "{:0>2}:{:0>2}".format(hours, minutes)

    def __write_meta__(self, month: canvas.Canvas):
        """
            Write the meta information to the top of the pdf
        """
        month.write_name(self.contract['name']) 
        month.write_contract_id(self.contract['contract_id']) 
        month.write_institute(self.contract['institute']) 
        month.write_weekly_hours(self.contract['weekly_hours']) 
        month.write_superior(self.contract['superior']) 
        month.write_working_days(self.contract['working_days']) 
        month.write_contract_start(self.contract['contract_start']) 
        month.write_contract_end(self.contract['contract_end']) 

    def __to_weekday__(self, day):
        if day == 0:
            return 'MO'
        if day == 1:
            return 'DI'
        if day == 2:
            return 'MI'
        if day == 3:
            return 'DO'
        if day == 4:
            return 'FR'
        if day == 5:
            return 'SA'
        if day == 6:
            return 'SO'

    def __get_month_year_list__(self, args):
        contract_start = self.contract.get('contract_start', datetime.today())
        contract_end = self.contract.get('contract_end', datetime.today())
        if args.all:
            start_month = contract_start.month
            end_month = contract_end.month
            start_year = contract_start.year
            end_year = contract_end.year
        else:
            if args.from_month:
                start_year = 2000 + int(args.from_month[3:])
                start_month = int(args.from_month[:2])
                if args.to_month:
                    end_year = 2000 + int(args.to_month[3:])
                    end_month = int(args.to_month[:2])
                else:
                    end_month = start_month
                    end_year = start_year
            elif args.month:
                start_year = 2000 + int(args.month[3:])
                end_year = start_year
                start_month = int(args.month[:2])
                end_month = start_month
        res = []
        for y in range(start_year, end_year + 1):
            if y == end_year:
                upper = end_month
            else:
                upper = 12
            if y == start_year:
                lower = start_month
            else:
                lower = 1
            for m in range(lower, upper + 1):
                res.append((m, y))
        return res
