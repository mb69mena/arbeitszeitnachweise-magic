from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from argparse import Namespace
import argparse
import yaml
import io
from os.path import isdir
from datetime import date, time, datetime
from PyPDF2 import PdfFileReader, PdfFileWriter, pdf
from io_util import read_contract
from logging import debug, info, warn, error, critical
from date_util import *

A4_LONG_SIDE = 11.69 * inch

class Merger:
    """
        This class helps with adding things to the pdfs
        by providing a canvas on call of begin_month(date) that
        has some extra features (see ArbeitsCanvas)
    """

    def __init__(self, args: argparse.Namespace):
        """ Create the Merger with the given CL arguments """
        self.__args__ = args
        self.__originals__ = {}
        self.__configs__ = {}
        self.__output__ = PdfFileWriter()
        debug("Created merger")

    def __read_config__(self, year: int):
        """
            Read the configuration for the given year.
            The configuration resides in '/data/[year]/config.yml.
        """
        configfile = "./data/" + str(year) + "/config.yml"
        with open(configfile) as f:
            config = yaml.load(f)
            debug("Read configuration for year %s", year)
            return config

    def begin_month(self, date: date, balance: int = 0) -> canvas.Canvas:
        """
            Create and return a ArbeitsCanvas for the given month
            IF pdfs for the given year exist. Throws an exception if not.
            Balance should be the balance after last month in hours * 60 + minutes.

            :raises ValueError: If there are no pdfs for the requested year
        """
        # Check if directory for the given year exists
        year = str(date.year)
        debug("Check data existence for year %s", year)
        if isdir("./data/" + year):
            debug("Data directory exists for year %s", year)
            # If the pdf for the given year has not been loaded, do it now
            if not year in self.__originals__:
                path_to_original = "./data/" + year + "/Arbeitszeitnachweise.pdf"
                debug("Loading pdf from '%s'", path_to_original)
                self.__originals__[year] = PdfFileReader(
                    open(path_to_original, "rb"))
                debug("Loaded pdf from '%s'", path_to_original)
            # If no config has been loaded for the current year, do that
            if not year in self.__configs__:
                self.__configs__[year] = self.__read_config__(date.year)
            # Create canvas for given month
            debug("Creating canvas for month %s", date.month)
            can = __ArbeitsCanvas__(
                self.__args__, self.__configs__[year], self.__output__,
                self.__originals__[year].getPage(date.month - 1), date,
                balance)
            return can
        else:
            raise ValueError(f"Month {date.month}/{year} can't be written no pdf in /data/!")

    def finalize(self):
        """
            Writes the pdf. Call after all month have been added and all canvases
            have been closed using __ArbeitsCanvas__.apply()
        """
        outputStream = open(self.__args__.output, "wb")
        self.__output__.write(outputStream)
        outputStream.close()


class __ArbeitsCanvas__(canvas.Canvas):
    """A canvas wrapper with extra features to work with
    our working time pdfs. Simply call apply() when done editing."""

    def __init__(self, args: argparse.Namespace, config: dict,
                 output: PdfFileWriter, original_page: pdf.PageObject,
                 date: date, balance: int):
        self.__args__ = args
        self.__date__ = date
        self.__start_balance__ = balance
        self.__config__ = config
        self.__packet__ = io.BytesIO()
        self.__output__ = output
        self.__original_page__ = original_page
        self.__quotas__ = {}
        # Initialize canvas
        super().__init__(self.__packet__, pagesize=A4)
        self.setFont(self.__args__.font, self.__args__.fontsize)

    def __write_data__(self, col_name: str, date: date, text: str):
        x = self.__config__['table'][col_name]
        y = self.__config__['table']['first_line']
        delta = self.__config__['table']['line_height']
        # Add offset to account for the temp_balance line
        line_offset = delta if date.day >= 16 else 0
        self.drawString(x,
                        __flip__(y) - (date.day - 1) * delta - line_offset,
                        text)

    def __write_balance_last_month__(self, balance_last_month: int):
        x = self.__config__['balance_last_month']['x']
        y = self.__config__['balance_last_month']['y']
        bal = __format_balance__(balance_last_month)
        self.drawRightString(x, __flip__(y), bal)

    def apply(self) -> int:
        """
            Finishes iriting and returns the balance in minutes after the current month.
        """
        balance = 0
        # Save canvas
        self.save()
        # Create PDF from packet object
        self.__packet__.seek(0)
        new_pdf_page = PdfFileReader(self.__packet__)
        # Merge pages and push to output
        self.__original_page__.mergePage(new_pdf_page.getPage(0))
        self.__output__.addPage(self.__original_page__)
        return balance

    def write_line(self,
                   date: date,
                   start: time,
                   end: time,
                   start_after_break: time = None,
                   end_after_break: time = None,
                   justification: str = "",
                   mandatory=False,
                   actual="",
                   quota="",
                   balance=""):
        """
            Write a line of data into the file. This function keeps track
            of the resulting balance and automatically calculates actual and
            quota working times.
            `date` is the line the data will be written to.
        """
        if start == end:
            self.__write_data__("balance", date, balance)
            return
        self.__write_data__("start", date, start.strftime("%H:%M"))
        self.__write_data__("end", date, end.strftime("%H:%M"))
        if start_after_break:
            self.__write_data__("start_after_break", date,
                                start_after_break.strftime("%H:%M"))
        if end_after_break:
            self.__write_data__("end_after_break", date,
                                end_after_break.strftime("%H:%M"))
        if justification:
            self.__write_data__("justification", date, justification)
        # Write actual working time
        self.__write_data__("actual", date, actual)
        # If the mandatory flag was set, the working times were mandatory
        # Add an entry for calculating balance later
        self.__write_data__('quota', date, quota)
        self.__write_data__('balance', date, balance)

    def write_date(self, col_name: str, date: date, text: str):
        """
            DEPRECATED
        """
        x = self.__config__['table'][col_name]
        y = self.__config__['table']['first_line']
        delta = self.__config__['table']['line_height']
        # Add offset to account for the temp balance line
        line_offset = delta if date.day >= 16 else 0
        self.drawString(x,
                        __flip__(y) - (date.day - 1) * delta - line_offset,
                        text)

    def write_name(self, name: str):
        x = self.__config__['name']['x']
        y = self.__config__['name']['y']
        self.drawRightString(x, __flip__(y), name)

    def write_contract_id(self, nr: str):
        x = self.__config__['contract_id']['x']
        y = self.__config__['contract_id']['y']
        self.drawRightString(x, __flip__(y), nr)

    def write_institute(self, institute: str):
        x = self.__config__['institute']['x']
        y = self.__config__['institute']['y']
        self.drawRightString(x, __flip__(y), institute)

    def write_weekly_hours(self, weekly_hours: str):
        x = self.__config__['weekly_hours']['x']
        y = self.__config__['weekly_hours']['y']
        self.drawRightString(x, __flip__(y), weekly_hours)

    def write_superior(self, superior: str):
        x = self.__config__['superior']['x']
        y = self.__config__['superior']['y']
        self.drawRightString(x, __flip__(y), superior)

    def write_working_days(self, working_days: str):
        x = self.__config__['working_days']['x']
        y = self.__config__['working_days']['y']
        self.drawRightString(x, __flip__(y), working_days)

    def write_contract_start(self, contract_start: date):
        x = self.__config__['contract_start']['x']
        y = self.__config__['contract_start']['y']
        date = contract_start.strftime("%d.%m.%Y")
        self.drawRightString(x, __flip__(y), date)

    def write_contract_end(self, contract_end: date):
        x = self.__config__['contract_end']['x']
        y = self.__config__['contract_end']['y']
        date = contract_end.strftime("%d.%m.%Y")
        self.drawRightString(x, __flip__(y), date)

    def write_temp_balance(self, temp_balance: str):
        x = self.__config__['table']['balance']
        y = self.__config__['temp_balance']['y']
        self.drawString(x, __flip__(y), temp_balance)

    def write_temp_actual(self, temp_actual: str):
        x = self.__config__['table']['actual']
        y = self.__config__['temp_balance']['y']
        self.drawString(x, __flip__(y), temp_actual)

    def write_temp_quota(self, temp_quota: str):
        x = self.__config__['table']['quota']
        y = self.__config__['temp_balance']['y']
        self.drawString(x, __flip__(y), temp_quota)

    def write_final_balance(self, final_balance: str):
        x = self.__config__['table']['balance']
        y = self.__config__['final_balance']['y']
        self.drawString(x, __flip__(y), final_balance)

    def write_final_actual(self, final_actual: str):
        x = self.__config__['table']['actual']
        y = self.__config__['final_balance']['y']
        self.drawString(x, __flip__(y), final_actual)

    def write_final_quota(self, final_quota: str):
        x = self.__config__['table']['quota']
        y = self.__config__['final_balance']['y']
        self.drawString(x, __flip__(y), final_quota)


def __flip__(y: float):
    """ Flips the given y coordinate on A4 """
    return A4_LONG_SIDE - y
