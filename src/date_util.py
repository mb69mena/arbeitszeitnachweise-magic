from datetime import date
from calendar import monthrange

# TODO: Move to more generic version
FILENAME = "/home/maurizio/arbeitszeitnachweise-magic/data/2019/feiertage"


def read_holidays():
    with open(FILENAME) as f:
        res = [line.split() for line in f]
        res = [event[0] for event in res if "x" in event]
        return res


def days_of_month(date: date):
    return monthrange(date.year, date.month)[1]


if __name__ == '__main__':
    for event in read_holidays():
        print(event)
